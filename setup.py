import io
import os
import re

from setuptools import setup, find_packages


def read(*names, **kwargs):
    file = os.path.join(os.path.dirname(__file__), *names)
    encoding = kwargs.get("encoding", "utf8")
    with io.open(file, encoding=encoding) as f:
        return f.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(
        r"^__version__ = ['\"]([^'\"]*)['\"]",
        version_file,
        re.M,
    )
    if version_match:
        return version_match.group(1)

    raise RuntimeError("Unable to find version string.")


setup(
    name='mangatraders',
    version=find_version('mangatraders', '__init__.py'),
    url='https://gitlab.com/julot/mangatraders',
    license='MIT',
    author='Andy Yulius',
    author_email='andy.julot@gmail.com',
    description='MangaTraders saved all-pages chapter zipper',
    long_description=read('README.rst'),
    keywords=' '.join([
        'mangatraders',
        'manga traders',
        'zipper',
        'packer',
    ]),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Intended Audience :: End Users/Desktop',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Utilities',
    ],
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'clicking',
        'click-help-colors',
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'tox'],
    entry_points={
        'console_scripts': [
            'mangatraders=mangatraders:cli',
        ],
    },
)
