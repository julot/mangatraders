from mangatraders.utils import valid_dirs


def test():
    sources = [
        'MangaTraders Zip 2017-07-21 11-40.log',
        'MangaTraders Zip 2017-07-21 11-42.log',
        'Swap Swap Chapter 1.htm',
        'Swap Swap Chapter 1.zip',
        'Swap Swap Chapter 1_files',
        'Tsuki Tsuki! Chapter 1.htm',
        'Tsuki Tsuki! Chapter 1.zip',
        'Tsuki Tsuki! Chapter 1_files',
        'Wrong Folder',
    ]
    targets = [
        'Swap Swap Chapter 1_files',
        'Tsuki Tsuki! Chapter 1_files',
    ]

    assert list(valid_dirs(iterable=sources)) == targets
