from mangatraders.utils import valid_files


def test():
    sources = ['0001-001.jpg', '0001-002.jpg', '0001-003.jpg', '0001-004.jpg',
               '0001-005.jpg', '0001-006.jpg', '0001-007.jpg', '0001-008.jpg',
               '0001-009.jpg', '0001-010.jpg', '0001-011.jpg', '0001-012.jpg',
               '0001-013.jpg', '0001-014.jpg', '0001-015.jpg', '0001-016.jpg',
               '0001-017.jpg', '0001-018.jpg', 'analytics.js',
               'bootstrap-social.css', 'bootstrap.css', 'bootstrap.js',
               'FileSaver.js', 'font-awesome.css', 'jquery.js',
               'jquery_002.js', 'jszip-utils.js', 'jszip.js', 'list.txt',
               'loader.gif', 'navbar.png', 'reqAd.jsp', 'reqAd_002.jsp',
               'smaatoAdTag.js']

    targets = ['0001-001.jpg', '0001-002.jpg', '0001-003.jpg', '0001-004.jpg',
               '0001-005.jpg', '0001-006.jpg', '0001-007.jpg', '0001-008.jpg',
               '0001-009.jpg', '0001-010.jpg', '0001-011.jpg', '0001-012.jpg',
               '0001-013.jpg', '0001-014.jpg', '0001-015.jpg', '0001-016.jpg',
               '0001-017.jpg', '0001-018.jpg']

    assert list(valid_files(iterable=sources)) == targets

    sources = ['0001-001.jpg', '0001-002.jpg', '0001-003.jpg', '0001-004.jpg',
               '0001-005.jpg', '0001-006.jpg', '0001-007.png', '0001-008.png',
               '0001-009.png', '0001-010.png', '0001-011.png', '0001-012.png',
               '0001-013.png', '0001-014.png', '0001-015.png', '0001-016.png',
               '0001-017.png', '0001-018.png', '0001-019.png', '0001-020.png',
               '0001-021.png', '0001-022.png', '0001-023.png', '0001-024.png',
               '0001-025.png', '0001-026.png', '0001-027.png', '0001-028.png',
               '0001-029.png', '0001-030.png', '0001-031.png', '0001-032.png',
               '0001-033.png', '0001-034.png', '0001-035.png', '0001-036.png',
               '0001-037.jpg', '0001-038.jpg', '0001-039.jpg', 'analytics.js',
               'bootstrap-social.css', 'bootstrap.css', 'bootstrap.js',
               'FileSaver.js', 'font-awesome.css', 'jquery.js',
               'jquery_002.js', 'jszip-utils.js', 'jszip.js', 'list.txt',
               'loader.gif', 'navbar.png', 'reqAd.jsp', 'reqAd_002.jsp',
               'smaatoAdTag.js']

    targets = ['0001-001.jpg', '0001-002.jpg', '0001-003.jpg', '0001-004.jpg',
               '0001-005.jpg', '0001-006.jpg', '0001-007.png', '0001-008.png',
               '0001-009.png', '0001-010.png', '0001-011.png', '0001-012.png',
               '0001-013.png', '0001-014.png', '0001-015.png', '0001-016.png',
               '0001-017.png', '0001-018.png', '0001-019.png', '0001-020.png',
               '0001-021.png', '0001-022.png', '0001-023.png', '0001-024.png',
               '0001-025.png', '0001-026.png', '0001-027.png', '0001-028.png',
               '0001-029.png', '0001-030.png', '0001-031.png', '0001-032.png',
               '0001-033.png', '0001-034.png', '0001-035.png', '0001-036.png',
               '0001-037.jpg', '0001-038.jpg', '0001-039.jpg']

    assert list(valid_files(iterable=sources)) == targets
