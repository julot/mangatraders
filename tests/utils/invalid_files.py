from mangatraders.utils import invalid_files, valid_files


def test():
    sources = ['0001-001.jpg', '0001-002.jpg', '0001-003.jpg', '0001-004.jpg',
               '0001-005.jpg', '0001-006.jpg', '0001-007.jpg', '0001-008.jpg',
               '0001-009.jpg', '0001-010.jpg', '0001-011.jpg', '0001-012.jpg',
               '0001-013.jpg', '0001-014.jpg', '0001-015.jpg', '0001-016.jpg',
               '0001-017.jpg', '0001-018.jpg', 'analytics.js',
               'bootstrap-social.css', 'bootstrap.css', 'bootstrap.js',
               'FileSaver.js', 'font-awesome.css', 'jquery.js',
               'jquery_002.js', 'jszip-utils.js', 'jszip.js', 'list.txt',
               'loader.gif', 'navbar.png', 'reqAd.jsp', 'reqAd_002.jsp',
               'smaatoAdTag.js', 'wrong.txt']

    targets = ('analytics.js', 'bootstrap-social.css', 'bootstrap.css',
               'bootstrap.js', 'FileSaver.js', 'font-awesome.css', 'jquery.js',
               'jquery_002.js', 'jszip-utils.js', 'jszip.js',
               'loader.gif', 'navbar.png', 'reqAd.jsp', 'reqAd_002.jsp',
               'smaatoAdTag.js')

    result = set(invalid_files(iterable=sources))
    assert result == set(targets)

    valid = set(valid_files(iterable=sources))
    assert set(sources) - result - valid == set(['list.txt', 'wrong.txt'])
