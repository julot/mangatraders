import os
import shutil
import zipfile

import click
import clicking

from click_help_colors import HelpColorsGroup

from mangatraders import utils


__version__ = '0.2.0'

HELP = """
    \b
    MangaTraders
    Version {0}
    
    Collection of convenience command for working with MangaTraders.
    
    """.format(__version__)

EPILOG = '(c) 2017 Andy Yulius <andy.julot@gmail.com>'


class Error(click.ClickException):

    def show(self, file=None):
        clicking.fail('Error: %s' % self.format_message())


def get_sub_dirs(path):
    clicking.working('Listing contents of "{0}"... '.format(path), nl=False)
    contents = os.listdir(path)
    clicking.success('OK')

    clicking.working(
        'Searching for sub directories from "{0}"... '.format(path),
        nl=False,
    )
    sub_dirs = list(utils.valid_dirs(iterable=contents))
    clicking.success('OK')
    clicking.progress('Get {0} sub directories.'.format(len(sub_dirs)))

    return sub_dirs


@click.group(
    cls=HelpColorsGroup,
    help_headers_color='yellow',
    help_options_color='green',
    help=HELP,
    epilog=EPILOG,
)
def cli():
    clicking.info('MangaTraders v{0}'.format(__version__))


# noinspection PyShadowingBuiltins
@cli.command(
    short_help='Pack saved all-page chapter into zip file.',
    epilog=EPILOG,
)
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=False,
        writable=True,
        resolve_path=True,
    ),
)
def zip(path):
    """
    Search for *_files directory in PATH then pack all the likely manga page
    images into *.zip file at PATH location.

    Before running this command, be sure to run check command first.

    """
    sub_dirs = get_sub_dirs(path=path)

    searching = 'Searching for image files from "{0}"... '

    for sub_dir in sub_dirs:
        clicking.working(searching.format(sub_dir), nl=False)
        files = utils.valid_files(os.listdir(os.path.join(path, sub_dir)))
        files = list(files)
        clicking.success('OK')
        clicking.progress('Found {0} image files.'.format(len(files)))

        filename = os.path.join(path, '{0}.zip'.format(sub_dir[:-6]))
        try:
            with click.progressbar(
                iterable=files,
                label='Zipping files...',
                show_eta=True,
            ) as bar:
                with zipfile.ZipFile(file=filename, mode='w') as f:
                    for file in bar:
                        f.write(
                            filename=os.path.join(path, sub_dir, file),
                            arcname=file,
                        )
        except Exception as e:
            clicking.fail('\n{0}'.format(str(e)))
            exc = Error('Zip operation failed. Please review the message.')
            raise exc
        else:
            clicking.progress('"{0}.zip" file created.'.format(sub_dir[:-6]))


@cli.command(
    short_help='Delete saved html files.',
    epilog=EPILOG,
)
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=False,
        writable=True,
        resolve_path=True,
    ),
)
def delete(path):
    """
    Search for *_files directory and it's html file in PATH then delete them.

    """
    sub_dirs = get_sub_dirs(path=path)

    for sub_dir in sub_dirs:
        clicking.working(
            'Deleting "{0}" file and directory... '.format(sub_dir[:-6]),
            nl=False,
        )
        try:
            os.unlink(path='{0}.htm'.format(os.path.join(path, sub_dir[:-6])))
            shutil.rmtree(os.path.join(path, sub_dir))
        except Exception as e:
            clicking.fail('Error')
            clicking.fail(str(e))
            exc = Error('Delete failed. Please review the message.')
            raise exc
        else:
            clicking.success('OK')


@cli.command(
    short_help='Check for possibly missed image files.',
    epilog=EPILOG,
)
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=False,
        writable=True,
        resolve_path=True,
    ),
)
def check(path):
    """
    Check PATH whether it is safe for zip operation or not.

    Get all file extension inside *_files directories in PATH,
    then compared it to image file extensions and excluded file extensions.
    If there's an extension not in both list,
    the program will exit with return code 1.

    \b
    Check return code:
    [Nix] $ echo $?
    [Win] > echo %errorlevel%

    """
    sub_dirs = get_sub_dirs(path=path)

    failed = False

    for sub_dir in sub_dirs:
        clicking.working(
            'Checking files in "{0}"... '.format(sub_dir),
            nl=False,
        )

        contents = set(os.listdir(os.path.join(path, sub_dir)))
        valid_files = set(utils.valid_files(iterable=contents))

        if not valid_files:
            failed = True
            clicking.fail('Error')
            clicking.fail("There's no image files.")
            continue

        contents -= valid_files
        invalid_files = set(utils.invalid_files(iterable=contents))
        contents -= invalid_files

        if contents:
            failed = True
            clicking.fail('Error')
            message = '{0} Missed files: "{1}".'
            clicking.fail(message.format(len(contents), '", "'.join(contents)))
            continue

        clicking.success('OK')

    if failed:
        exc = Error("There's something wrong. Please review the message.")
        raise exc
