import os


IMAGE_EXTS = ('.jpg', '.jpeg', '.png', '.gif', '.tif', '.tiff', '.bmp')

EXCLUDED_EXTS = ('.css', '.js', '.jsp')

EXCLUDED_IMAGES = ('loader.gif', 'navbar.png')


def valid_dirs(iterable):
    """Return filtered iterable whose value ends with "_files"."""
    for item in iterable:
        if not item.endswith('_files'):
            continue

        yield item


def valid_files(iterable):
    """
    Return filtered iterable whose value ends with image extensions.

    The valid image extension is .png, .jpg, .jpeg, .gif, .tif, .tiff & .bmp.

    """
    for item in iterable:
        if item.lower() in EXCLUDED_IMAGES:
            continue

        root, ext = os.path.splitext(item)
        if ext.lower() not in IMAGE_EXTS:
            continue

        yield item


def invalid_files(iterable):
    """
    Return filtered iterable whose value ends with excluded extensions or
    value is in excluded images.

    The excluded extensions is .css, .js & .jsp.
    The excluded images is loader.gif & navbar.png.

    """
    for item in iterable:
        if item.lower() in EXCLUDED_IMAGES:
            yield item
            continue

        root, ext = os.path.splitext(item)
        if ext.lower() in EXCLUDED_EXTS:
            yield item
            continue
