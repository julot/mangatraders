############
mangatraders
############

Collection of convenience command for working with MangaTraders.


Installation
============

.. code-block:: bat

  (.venv) > pip install git+https://gitlab.com/julot/mangatraders.git#egg=mangatraders


Usage
=====

.. code-block:: bat

  (.venv) > mangatraders --help


Changes
=======

0.1.0
-----

* First public release.
